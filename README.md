program pobierający i wyświetlający kursy walut z banku NBP.


Pobierane dane z NBP to: kurs kupna oraz kurs sprzedaży.


Podczas uruchomienia programu, użytkownik podaje następujące dane:

kod waluty (USD, EUR, CHF, GBP)

przedział czasu dla jakiego zostaną pobrane dane – data początkowa i data końcowa



W wyniku działania programu użytkownik otrzymuje:

obliczony średnik kurs kupna dla podanych danych

obliczone odchylenie standardowe kursów sprzedaży dla podanych danych


Dodatkowe wymagania:

dane pobierane jak w przykładzie poniżej

dane wyświetlane na standardowym wyjściu

pliki umieszczone w pakiecie pl.parser.nbp

metoda 'main' umieszczona w klasie o nazwie 'MainClass'

Informacje i wskazówki:

ilość klas dowolna

niezbędne informacje do pobierania danych z NBP na stronie: http://www.nbp.pl/home.aspx?f=/kursy/instrukcja_pobierania_kursow_walut.html

przykładowy plik XML zawierający kursy: http://www.nbp.pl/kursy/xml/c073z070413.xml

sposób parsowania dokumentów XML dowolny

jako datę należy brać pole 'data_publikacji' z pliku XML

kursy z daty początkowej i końcowej również mają być brane pod uwagę


W przypadku korzystania z zewnętrznych bibliotek, wymagane jest utworzenie projektu maven'owego wraz z dodanymi zależnościami w pliku pom.xml


Uruchomienie programu z przykładowymi poprawnymi danymi wejściowymi:

java pl.parser.nbp.MainClass EUR 2013-01-28 2013-01-31


Poprawne dane wyjściowe dla powyżej przedstawionego przykładowego wywołania:

4,1505

0,0125


Opis danych wejściowych i wyjściowych:

EUR › kod waluty

2013-01-28 › data początkowa

2012-01-31 › data końcowa

4,1505 › średni kurs kupna

0,0125 › odchylenie standardowe kursów sprzedaży