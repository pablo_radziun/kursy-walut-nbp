package pl.parser.nbp;

class Calculation {

    private String currency;
    private String startDay;
    private String endDay;
    private float bidMean;
    private double askStdDeviation;

    //Constructor

    Calculation(String currency, String startDay, String endDay) {
        this.currency = currency;
        this.startDay = startDay;
        this.endDay = endDay;
    }

    //Setters and Getters

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getStartDay() {
        return startDay;
    }

    public void setStartDay(String startDay) {
        this.startDay = startDay;
    }

    public String getEndDay() {
        return endDay;
    }

    public void setEndDay(String endDay) {
        this.endDay = endDay;
    }


//methods

    //Metoda wywołująca inne metody niezbędne do otrzymania danych wyjściowych i wyświetlająca wejście i wyjście programu
    void getCalculation() {
        System.out.println("Input: " + currency + " " + startDay + " " + endDay);
        System.out.print("Output: ");
        getBidMean();
        getAskStdDeviation();
        System.out.print(bidMean + " ");
        System.out.print(askStdDeviation);
    }

    //Metoda obliczająca wartość średnią elementów listy bidList (średni kurs kupna podanej waluty z zadanego  okresu)
    private float getBidMean() {

        NbpXmlParser bidParser = new NbpXmlParser();
        bidParser.getBidData(currency, startDay, endDay);

        float sum = 0;

        for (int i = 0; i < bidParser.bidList.size(); i++) {
            sum = sum + Float.valueOf(bidParser.bidList.get(i));
        }

        float avg = sum / bidParser.bidList.size();

        this.bidMean = avg;

        return avg;
    }

    // Metoda obliczająca odchylenie standardwe elementów listy askList (odchylenie kursu sprzedaży podanej waluty z zadanego okresu)
    private double getAskStdDeviation() {
        NbpXmlParser askParser = new NbpXmlParser();
        askParser.getAskData(currency, startDay, endDay);

        // obliczenie średniej
        double sum = 0;

        for (int i = 0; i < askParser.askList.size(); i++) {
            sum = sum + Double.valueOf(askParser.askList.get(i));
        }

        double avg = sum / askParser.askList.size();

        // Obliczenie odchylenie standardowego

        sum = 0;
        for (int i = 0; i < askParser.askList.size(); i++) {
            sum = sum + Math.pow((Double.valueOf(askParser.askList.get(i)) - avg), 2);
        }

        double stdDev = Math.sqrt((sum / askParser.askList.size()));


        this.askStdDeviation = stdDev;

        return stdDev;
    }
}






