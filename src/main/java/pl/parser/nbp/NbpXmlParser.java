package pl.parser.nbp;


import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Objects;

class NbpXmlParser {

    // Lista przechowująca kursy kupna podanej waluty z zadanego okresu
    ArrayList<String> bidList = new ArrayList<>();

    // Lista przechowująca kursy sprzedaży podanej waluty z zadanego okresu
    ArrayList<String> askList = new ArrayList<>();


    NbpXmlParser() {
    }

    // Metoda pobierająca kursy sprzedaży podanej waluty i okresu. Parsuje xml i zapisuje dane do ArrayListy

    void getAskData(String currency, String startDay, String endDay)

    {

        String type = "Ask";

        try {
            readData(currency, startDay, endDay, type);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // Metoda pobierająca kursy kupna podanej waluty i okresu. Parsuje xml i zapisuje dane do ArrayListy

    void getBidData(String currency, String startDay, String endDay) {
        String type = "Bid";

        try {
            readData(currency, startDay, endDay, type);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //Metoda tworząca URL i pobierająca z Api NBP kursy kupna lub sprzedaży podanej waluty z podanego okresu w formacie XML
    private void readData(String currency, String startDay, String endDay, String type) throws Exception {
        URL url = new URL("http://api.nbp.pl/api/exchangerates/rates/c/" + currency + "/" + startDay + "/" + endDay + "?format=xml");
        URLConnection connection = url.openConnection();

        Document doc = parseXML(connection.getInputStream());
        NodeList descNodes = doc.getElementsByTagName(type);
        if (Objects.equals(type, "Bid")) {
            {
                for (int i = 0; i < descNodes.getLength(); i++) {
                    bidList.add(descNodes.item(i).getTextContent());

                }
            }
        } else {
            for (int i = 0; i < descNodes.getLength(); i++) {
                askList.add(descNodes.item(i).getTextContent());
            }
        }
    }

    // Metoda parsująca XML
    private Document parseXML(InputStream stream)
            throws Exception {
        DocumentBuilderFactory objDocumentBuilderFactory = null;
        DocumentBuilder objDocumentBuilder = null;
        Document doc = null;
        try {
            objDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
            objDocumentBuilder = objDocumentBuilderFactory.newDocumentBuilder();

            doc = objDocumentBuilder.parse(stream);
        } catch (Exception ex) {
            throw ex;
        }

        return doc;

    }


}
